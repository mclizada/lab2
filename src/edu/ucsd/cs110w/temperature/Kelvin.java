/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (mclizada): write class javadoc
 *
 * @author mclizada
 */
public class Kelvin extends Temperature {
	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
		return "" + this.getValue() + " K";
	}
	public Temperature toCelsius() {
		float kelv = this.getValue();
		float cels = kelv - 273;
		Temperature converted = new Celsius(cels);
		return converted;
		
	}
	public Temperature toFahrenheit(){
		float input = this.getValue();
		Temperature out = new Kelvin(input);
		Temperature converted = out.toCelsius();
		input = converted.getValue();
		input = ((input*9)/5)+32;
		Temperature conv2 = new Fahrenheit(input);
		return conv2;
		
		
	}
	public Temperature toKelvin(){
		return this;
	}

}
