package edu.ucsd.cs110w.temperature;

public class Fahrenheit extends Temperature {
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		return "" + this.getValue() + " F";
	}
	@Override
	public Temperature toCelsius() {
		float fahr = this.getValue();
		float cels = ((fahr-32)*5)/9;
		Temperature converted = new Celsius(cels);
		return converted;
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
	
	public Temperature toKelvin(){
		float fahr = this.getValue();
		float cels = ((fahr-32)*5)/9;
		float kelv = cels + 273;
		Temperature converted = new Kelvin(kelv);
		return converted;
	
	}
}
