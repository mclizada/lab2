
package edu.ucsd.cs110w.temperature;

public class Celsius extends Temperature {
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		return "" + this.getValue() + " C";
	}
	@Override
	public Temperature toCelsius() {
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		float cels = this.getValue();
		float fahr = ((cels*9)/5 + 32);
		Temperature converted = new Fahrenheit(fahr);
		return converted;
	}
	public Temperature toKelvin(){
		float cels = this.getValue();
		float kelv = cels + 273;
		Temperature converted = new Kelvin(kelv);
		return converted;
	}

}
