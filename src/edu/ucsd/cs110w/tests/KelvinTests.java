package edu.ucsd.cs110w.tests;

import junit.framework.TestCase;
import edu.ucsd.cs110w.temperature.Fahrenheit;
import edu.ucsd.cs110w.temperature.Kelvin;
import edu.ucsd.cs110w.temperature.Temperature;

public class KelvinTests extends TestCase {
	private float delta = 0.01f;

	public void testKelvin() {
		float value = 12.34f;
		Kelvin temp = new Kelvin(value);
		assertEquals(value, temp.getValue(), delta);
	}

	public void testKelvinToString() {
		float value = 12.34f;
		Kelvin temp = new Kelvin(value);
		String string = temp.toString();
		String beginning = "" + value;
		String ending = " K";
		// Verify the suffix of the formatted string
		assertTrue(string.startsWith(beginning));
		// Verify the prefix of the formatted string
		assertTrue(string.endsWith(ending));
		// Verify the middle of the formatted string
		int endIndex = string.indexOf(ending);
		// (Hint: what is the length of the middle of the string?)
		assertTrue(string.substring(0, endIndex).equals(beginning));
	}

	public void testKelvinToFahrenheit() {
		Kelvin temp = new Kelvin(273);
		Temperature convert = temp.toFahrenheit();
		assertEquals(32, convert.getValue(), delta);
	}

	public void testKelvinToCelsius() {
		Kelvin temp = new Kelvin(273);
		Temperature convert = temp.toCelsius();
		assertEquals(0, convert.getValue(), delta);
		temp = new Kelvin(373);
		convert = temp.toCelsius();
		assertEquals(100, convert.getValue(), delta);
	}
	public void testKelvinToKelvin(){
		Kelvin temp = new Kelvin(32);
		Temperature convert = temp.toKelvin();
		assertEquals(32, convert.getValue(), delta);
	}
}
