package edu.ucsd.cs110w.tests;

import junit.framework.TestCase;
import edu.ucsd.cs110w.temperature.Celsius;
import edu.ucsd.cs110w.temperature.Temperature;

public class CelsiusTests extends TestCase {
	private float delta = 0.001f;
	public void testCelsius(){
		float value = 12.34f;
		Celsius temp = new Celsius(value);
		assertEquals(value, temp.getValue(), delta);
	}
	public void testCelsiusToString(){
		float value = 12.34f;
		Celsius temp = new Celsius(value);
		String string = temp.toString();
		String beginning = "" + value;
		String ending = " C";
		assertTrue(string.startsWith(beginning));
		assertTrue(string.endsWith(ending));
		int endIndex = string.indexOf(ending);
		assertTrue(string.substring(0,endIndex).equals(beginning));
	}
	public void testCelsiusToCelsius()
	{
		Celsius temp = new Celsius(0);
		Temperature convert = temp.toCelsius();
		assertEquals(0, convert.getValue(), delta);
	}
	public void testCelsiusToFahrenheit(){
		Celsius temp = new Celsius(0);
		Temperature convert = temp.toFahrenheit();
		assertEquals(32,convert.getValue(), delta);
		temp = new Celsius(100);
		convert = temp.toFahrenheit();
		assertEquals(212,convert.getValue(), delta);
	}
	public void testCelsiusToKelvin(){
		Celsius temp = new Celsius(0);
		Temperature convert = temp.toKelvin();
		assertEquals(273,convert.getValue(), delta);
		temp = new Celsius(100);
		convert = temp.toKelvin();
		assertEquals(373,convert.getValue(), delta);
	}
}


